# Project

alternating-diodes-on-pwm is a nice project which uses only peripherals to get the effect.
It alternates three outputs simulating step motor like driver.
The speed of alternating can be tuned with potentiometer.

Project is tightly coupled with STM32F401x [reference](https://www.st.com/content/ccc/resource/technical/document/reference_manual/5d/b1/ef/b2/a1/66/40/80/DM00096844.pdf/files/DM00096844.pdf/jcr:content/translations/en.DM00096844.pdf).  
It was written in a way to reflect the documentation not to create its own language.
Because of it it is mostly written in bit *offsets* and *names* straight from *reference*.

# Running

## prerequisites

- cmake
- arm-none-eabi
- make
- gdb
- openocd

## execution

```bash
cmake ./
make
# use correct configuration file if debugging on different hardware
openocd -f interface/stlink-v2-1.cfg -f target/stm32f4x.cfg -c init -c "reset init"
# it will also load the file to memory as in .gdbinit
arm-none-eabi-gdb
```

# Pinout

*To Do*

# Presentation

[STM32 Timers](https://gitlab.com/oneacik/lifecycle/-/tree/master/prezentacje%20i%20warsztaty%2Fpresentation%2FSTM32%20-%20TIMERS)
