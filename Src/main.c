/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

static void MX_GPIO_Init(void);

static void TIMx_Init(
        void *timerAddr,
        uint8_t compareValueAddrOffset,
        uint8_t compareModeAddrOffset,
        uint16_t enable,
        uint16_t enable2,
        uint16_t slaveMode,
        uint16_t prescaler,
        uint32_t reload,
        uint32_t counter,
        uint32_t compareValue,
        uint16_t compareMode,
        uint16_t outputMode
);

static void MX_TIM2_Init(void);

static void MX_TIM3_Init(void);

static void MX_TIM4_Init(void);

static void MX_TIM5_Init(void);

static void ADC1_INIT(void);

static void DMA_INIT();

static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void) {
    /* USER CODE BEGIN 1 */

    /* USER CODE END 1 */


    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();

    *((uint32_t *) 0xE0042008) |= 0b1111u; // TIM2-5 STOP

    MX_TIM3_Init();
    MX_TIM4_Init();
    MX_TIM5_Init();
    MX_TIM2_Init();

    DMA_INIT();
    ADC1_INIT();

    //MX_USART2_UART_Init();
    /* USER CODE BEGIN 2 */

    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1) {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
    }
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void) {
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** Configure the main internal regulator output voltage
    */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = 16;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                  | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
        Error_Handler();
    }
}
//84MHZ / 8400 = 10khz
#define TIM2_PRESCALER 875
#define TIM2_RELOAD 8
/*
 * OUR NEEDED SPEED RANGE = from 1hz to 256 hz
 * //ADC RESOLUTION = 2^8bit = 256values
 * 84 000 000 / x / 256 = 1
 * x = 328125
 * a*b = 328125
 *
 * 3*5*5*5 * 5*5*5*7
 * 375 * 875
 *
 */

#define TIMx_ENABLE  0b1u << 5u | 0b1u  // downcounter + enable

#define TIMx_COMPARE_VALUE 0
#define TIMx_RELOAD_VALUE 2
#define TIMx_PRESCALER_VALUE 375
#define TIMx_ENABLE_2 0b0

static void TIMx_Init(
        void *timerAddr,
        uint8_t compareValueAddrOffset,
        uint8_t compareModeAddrOffset,
        uint16_t enable,
        uint16_t enable2,
        uint16_t slaveMode,
        uint16_t prescaler,
        uint32_t reload,
        uint32_t counter,
        uint32_t compareValue,
        uint16_t compareMode,
        uint16_t outputMode
) {
    uint16_t *TIM2_ENABLE = ((uint16_t * )(timerAddr + 0x00));
    uint16_t *TIM2_ENABLE_2 = ((uint16_t * )(timerAddr + 0x04));
    uint16_t *TIM2_SLAVE_MODE = ((uint16_t * )(timerAddr + 0x08));
    uint16_t *TIM2_UPDATE = ((uint16_t * )(timerAddr + 0x14));
    uint16_t *TIM2_COMPARE_MODE = ((uint16_t * )(timerAddr + compareModeAddrOffset));
    uint16_t *TIM2_COMPARE_OUTPUT_MODE = ((uint16_t * )(timerAddr + 0x20));
    uint32_t *TIM2_COUNTER_VALUE = ((uint32_t * )(timerAddr + 0x24));
    uint16_t *TIM2_PRESCALER_VALUE = ((uint16_t * )(timerAddr + 0x28));
    uint32_t *TIM2_RELOAD_VALUE = ((uint32_t * )(timerAddr + 0x2C));
    uint32_t *TIM2_COMPARE_VALUE = ((uint32_t * )(timerAddr + compareValueAddrOffset));


    *TIM2_ENABLE = enable; // autoreload + counter enabled - at the end as I am scared
    *TIM2_ENABLE_2 = enable2; // Using update as external trigger

    *TIM2_SLAVE_MODE = slaveMode;

    *TIM2_PRESCALER_VALUE = prescaler - 1;           //down to 10khz
    *TIM2_RELOAD_VALUE = reload;              //down to 1hz for PWM

    *TIM2_COMPARE_VALUE = compareValue;               //half of it
    *TIM2_COMPARE_MODE = compareMode; // PWM1 MODE of compare + preload enabled (!?)
    *TIM2_COMPARE_OUTPUT_MODE = outputMode; // ENABLE OUTPUT

    *TIM2_UPDATE |= 1u; //whatever, lets just update

    *TIM2_COUNTER_VALUE = counter; // counter value is not preloaded
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void) {
    __HAL_RCC_TIM2_CLK_ENABLE();
    TIMx_Init(
            (void *) 0x40000000,
            0x34,
            0x18,
            TIMx_ENABLE, // autoreload + counter enabled
            0b010u << 4u, // Using update as external trigger
            0b0, // I'm master, not slave
            TIM2_PRESCALER, // PRESCALING AS A PART
            TIM2_RELOAD, // THIS VALUE WILL BE TAKEN FROM ADC SOON
            TIM2_RELOAD, // IT COULD BE EVEN 0, it doesn't matter
            TIM2_RELOAD / 2, // half on, half dead
            0b110u << 4u | 0b1u << 3u, // PWM1 MODE of compare + preload enabled (!?)
            0b1); // Enable output for ch1
}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void) {
    __HAL_RCC_TIM3_CLK_ENABLE();
    TIMx_Init(
            (void *) 0x40000400,
            0x3C,
            0x1C,
            TIMx_ENABLE, // autoreload + counter enabled
            TIMx_ENABLE_2,  // Nothing to see here
            0b1u << 7u | 0b001u << 4u | 0b111u, // SLAVE MODE + ITR1 as TIM2CH2 TRGO + External clock
            TIMx_PRESCALER_VALUE, // PRESCALING AS A PART
            TIMx_RELOAD_VALUE, // 3 diodes,
            TIMx_RELOAD_VALUE * 0 / 2, // this one is first,
            TIMx_COMPARE_VALUE, // 1 diode only on.
            0b110u << 4u | 0b1u << 3u, // PWM1 MODE of compare + preload enabled (!?) FOR CHANNEL 3
            0b1u << 8u); // ENABLE OUTPUT FOR CH3
}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void) {
    __HAL_RCC_TIM4_CLK_ENABLE();

    TIMx_Init(
            (void *) 0x40000800,
            0x38,
            0x18,
            TIMx_ENABLE, // autoreload + counter enabled
            TIMx_ENABLE_2,  // Nothing to see here
            0b1u << 7u | 0b001u << 4u | 0b111u, // SLAVE MODE + ITR1 as TIM2CH2 TRGO + External clock
            TIMx_PRESCALER_VALUE, // PRESCALING AS A PART
            TIMx_RELOAD_VALUE, // 3 diodes,
            TIMx_RELOAD_VALUE * 1 / 2, // this one is second,
            TIMx_COMPARE_VALUE, // 1 diode only on.
            0b110u << 12u | 0b1u << 11u, // PWM1 MODE of compare + preload enabled (!?) FOR CHANNEL 2
            0b1u << 4u); // ENABLE OUTPUT FOR CH2
}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void) {
    __HAL_RCC_TIM5_CLK_ENABLE();

    TIMx_Init(
            (void *) 0x40000C00,
            0x38,
            0x18,
            TIMx_ENABLE, // autoreload + counter enabled
            TIMx_ENABLE_2,  // Nothing to see here
            0b1u << 7u | 0b000u << 4u | 0b111u, // SLAVE MODE + ITR0 as TIM2CH2 TRGO + External clock //DIFFERENT!
            TIMx_PRESCALER_VALUE, // PRESCALING AS A PART
            TIMx_RELOAD_VALUE, // 3 diodes,
            TIMx_RELOAD_VALUE * 2 / 2, // this one is third,
            TIMx_COMPARE_VALUE, // 1 diode only on.
            0b110u << 12u | 0b1u << 11u, // PWM1 MODE of compare + preload enabled (!?) FOR CHANNEL 2
            0b1u << 4u); // ENABLE OUTPUT FOR CH2
}

static void ADC1_INIT() {
    __HAL_RCC_ADC1_CLK_ENABLE();

    void *ADC1_ADDR = (void*) 0x40012000;
    uint32_t *ADC_SR = (uint32_t * )(ADC1_ADDR + 0x00);
    uint32_t *ADC_CR1 = (uint32_t * )(ADC1_ADDR + 0x04); // Control Register
    uint32_t *ADC_CR2 = (uint32_t * )(ADC1_ADDR + 0x08);
    uint32_t *ADC_SAMPLE = (uint32_t * )(ADC1_ADDR + 0x0C); // Sampling time for DMA
    uint32_t *ADC_SQR1 = (uint32_t * )(ADC1_ADDR + 0x2C); // Sequence register
    uint32_t *ADC_SQR3 = (uint32_t * )(ADC1_ADDR + 0x34); // Sequence register

    *ADC_SQR1 = 0b0; // only one conversion - first one.
    *ADC_SQR3 = 0b1011; // first conversion is from 11 channel
    *ADC_CR1 = 0b10u << 24u | 0b1u << 8u; // 8bit resolution + Scan Mode (default mode)
    *ADC_CR2 = 0b1u << 8u | 0b1u << 1u | 0b1u; // DMA ON + continous conversion + Conventer ON
    *ADC_SAMPLE = 0b111; // longest sampling time for DMA on channel 0 - why not.
    *ADC_CR2 |= 0b1u << 30u; // start conversion of regular channels = it has to be set after converter on ¯\_(ツ)_/¯
}

static void DMA_INIT() {
    __HAL_RCC_DMA2_CLK_ENABLE();

    void *DMA2_ADDR = (void*) 0x40026400;
    uint8_t CHANNEL_NUM = 0; // Channel 0
    uint32_t *DMA_STREAM_CONF = DMA2_ADDR + 0x10 + 0x18 * CHANNEL_NUM;
    uint32_t *DMA_SNDTR = DMA2_ADDR + 0x14 + 0x18 * CHANNEL_NUM;

    uint32_t *DMA_FROM_MEMORY = DMA2_ADDR + 0x18 + 0x18 * CHANNEL_NUM; // ADC1 ADDR
    uint32_t *DMA_TO_MEMORY = DMA2_ADDR + 0x1C + 0x18 * CHANNEL_NUM; // TIM2 RELOAD

    *DMA_STREAM_CONF =
            0b10u << 13u | 0b10u << 11u | 0b1u << 5u; // 32 bit size of transfer + Peripheral is the flow controller
    *DMA_SNDTR = 0b1u; // One item to transfer

    *DMA_FROM_MEMORY = 0x4001204C; // ADC1 ADDR
    *DMA_TO_MEMORY = 0x4000002C; // TIM2 RELOAD

    *DMA_STREAM_CONF |= 0b1u; // enable stream
}


/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void) {

    /* USER CODE BEGIN USART2_Init 0 */

    /* USER CODE END USART2_Init 0 */

    /* USER CODE BEGIN USART2_Init 1 */

    /* USER CODE END USART2_Init 1 */
    huart2.Instance = USART2;
    huart2.Init.BaudRate = 115200;
    huart2.Init.WordLength = UART_WORDLENGTH_8B;
    huart2.Init.StopBits = UART_STOPBITS_1;
    huart2.Init.Parity = UART_PARITY_NONE;
    huart2.Init.Mode = UART_MODE_TX_RX;
    huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart2.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart2) != HAL_OK) {
        Error_Handler();
    }
    /* USER CODE BEGIN USART2_Init 2 */

    /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void) {
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin : B1_Pin */
    GPIO_InitStruct.Pin = B1_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void) {
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
